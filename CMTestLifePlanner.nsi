; File da includere
	!include "MUI2.nsh"
	
; Dati applicazione
	!define APPNAME "CM Test Life Planner"
	Name "${APPNAME}"
	!define COMPANYNAME "ARCA Technologies"
	!define VERSIONMAJOR 1
	!define VERSIONMINOR 0
	!define VERSIONBUILD 0

; Impostazione cartelle di sorgente, output ed installazione
	!define INSTFOLDER "C:\ARCA\CMTestLifePlanner"
	!define SOURCEFOLDER "D:\Progetti VBNET\CM18\CMTestLifePlanner\bin\Debug"
	OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
	
; Impostazione Icona installazione/disinstallazione
	!define MUI_ICON "D:\Progetti VBNET\NSISimages\icona.ico"
	!define MUI_UNICON "D:\Progetti VBNET\NSISimages\icona.ico"
	
	!define MUI_ABORTWARNING
	
; Impostazione header (bmp 150x57 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define MUI_HEADERIMAGE_BITMAP "D:\Progetti VBNET\NSISimages\headerlogo.bmp"
	!define MUI_HEADERIMAGE
	
; Impostazione pagina di benvenuto (testo ed immagine bmp 164x314 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define txtMessageLine1 "Installazione software di pianificazione test di vita"
	!define txtMessageLine2 "Utilizzabile con tutta la gamma CMxx"
	!define txtMessageLine3 "Utilizzare il file excel per una programmazione semplificata"
	!define MUI_WELCOMEPAGE_TITLE "Installazione SW CM Test Life Planner"
	!define MUI_WELCOMEPAGE_TEXT "${txtMessageLine1}$\r$\n$\r$\n${txtMessageLine2}$\r$\n$\r$\n${txtMessageLine3}"
	!define MUI_WELCOMEFINISHPAGE_BITMAP "D:\Progetti VBNET\NSISimages\LTWelcome.bmp"
	!insertmacro MUI_PAGE_WELCOME
	
; Impostazione pagina della licenza
	!insertmacro MUI_PAGE_LICENSE "D:\Progetti VBNET\NSISimages\ArcaLicense.txt"
	
; Personalizzazione cartella d'installazione
	;!insertmacro MUI_PAGE_DIRECTORY
	
	
	!insertmacro MUI_PAGE_COMPONENTS
	
; Visualizzazione avanzamento installazione file
	!insertmacro MUI_PAGE_INSTFILES

	; Impostazione conferma di disinstallazione
	!insertmacro MUI_UNPAGE_CONFIRM
	
; Visualizzazione avanzamento disinstallazione file
	!insertmacro MUI_UNPAGE_INSTFILES
	
; Impostazione linguaggio d'installazione
	!insertmacro MUI_LANGUAGE "English"
	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Titolo pagina finale"
	;!define MUI_FINISHPAGE_TEXT "Testo pagina finale"
	;!insertmacro MUI_PAGE_FINISH
	

Section "SW CM Test Life"
	SetOutPath "${INSTFOLDER}"
	SectionIn RO
		File "${SOURCEFOLDER}\CMCommand.dll"
		File "${SOURCEFOLDER}\CMLink.dll"
		File "${SOURCEFOLDER}\CMTrace.dll"
		File "${SOURCEFOLDER}\CMTestLifePlanner.exe"
		File "${SOURCEFOLDER}\ReplyCode.ini"
		File "${SOURCEFOLDER}\setup.ini"
		File "${SOURCEFOLDER}\testlist.ini"
		CreateShortCut "$DESKTOP\${APPNAME}.lnk" "C:\ARCA\CMTestLifePlanner\CMTestLifePlanner.exe" ""
		WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd

Section "File Excel pianificazione"
	;Banner::show /set 76 "Banner Visualizzato" /set 54 "Normal Text"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\ManageTestListFile.xlsm"
SectionEnd

Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd