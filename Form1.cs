﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CMUtils;
using myArcaNameSpace;
using System.Diagnostics;

namespace CMTestLifePlanner
{
    public partial class Form1 : Form
    {
        IniFile testList;
        string testListFileName = "";
        IniFile setup;
        CMCommand cmCommand;
        Utils utils;
        ArcaMessages message;
        bool stopTest = true;
        int totalCicle = 0;
        int actualCicle = 0;
        struct STest
        {
            public string title;
            public string description;
            public string[] parameters;
            public string command;
            public string answer;
            public int timeout;
            public int pause;
        }
        List<STest> cmTestList = new List<STest>();
        public Form1()
        {
            InitializeComponent();
            InitializeMyComonents();
        }

        void InitializeMyComonents()
        {
            setup = new IniFile(Application.StartupPath + "\\setup.ini");
            testListFileName = setup.GetValue("options", "filetest", ";");
            testList = new IniFile(Application.StartupPath + "\\" + testListFileName);
            cmCommand = new CMCommand();
            utils = new Utils();
            message = new ArcaMessages(lblMessage, this, null, lblTitle);
            int i = 1;
            while (testList.GetValue("test" + i, "title") != "")
            {
                STest tempTest = new STest();
                tempTest.title = testList.GetValue("test" + i, "title");
                tempTest.description = testList.GetValue("test" + i, "description");
                tempTest.parameters = testList.GetAllValues("test" + i, "parameters");
                tempTest.command = testList.GetValue("test" + i, "command");
                tempTest.answer = testList.GetValue("test" + i, "answer");
                tempTest.timeout = testList.GetInteger("test" + i, "timeout");
                tempTest.pause = testList.GetInteger("test" + i, "pause");
                cmTestList.Add(tempTest);
                i += 1;
            }
            cmCommand.CONNECTIONPARAM.ConnectionMode = CMCommand.DLINK_MODE_USB;
            if (setup.GetValue("connection","mode").ToUpper()!="USB")
            {
                cmCommand.CONNECTIONPARAM.ConnectionMode = CMCommand.DLINK_MODE_SERIAL;
                cmCommand.CONNECTIONPARAM.pRsConf.baudrate = 9600;
                cmCommand.CONNECTIONPARAM.pRsConf.car = 8;
                cmCommand.CONNECTIONPARAM.pRsConf.parity = 0;
                cmCommand.CONNECTIONPARAM.pRsConf.stop = 1;
                cmCommand.CONNECTIONPARAM.pRsConf.device = setup.GetValue("connection", "comportname").ToUpper();
            }
            actualCicle = testList.GetInteger("testparameters", "actualciclenumber");
            totalCicle = testList.GetInteger("testparameters", "lastciclenumber");
            lblCicleNumber.Text = actualCicle + " / " + totalCicle;
            lblTestListFileName.Text = setup.GetValue("options", "filetest", ";");
        }

        void FillTreeView()
        {
            TreeNode node;
            TreeNode childNode;
            
            for (int i = 0; i < cmTestList.Count(); i++)
            {
                node = new TreeNode(cmTestList[i].title);
                tvTestList.Nodes.Add(node);
                childNode = new TreeNode("description:" + cmTestList[i].description);
                tvTestList.Nodes[node.Index].Nodes.Add(childNode);
                //for (int x = 0; x < cmTestList[i].parameters.Count(); x++)
                //{
                //    childNode = new TreeNode("parameters:" + cmTestList[i].parameters[x]);
                //    tvTestList.Nodes[node.Index].Nodes.Add(childNode);
                //}
                childNode = new TreeNode("command:" + cmTestList[i].command);
                tvTestList.Nodes[node.Index].Nodes.Add(childNode);
                childNode = new TreeNode("answer:" + cmTestList[i].answer);
                tvTestList.Nodes[node.Index].Nodes.Add(childNode);
                childNode = new TreeNode("timeout (ms):" + cmTestList[i].timeout);
                tvTestList.Nodes[node.Index].Nodes.Add(childNode);
                childNode = new TreeNode("pause (ms):" + cmTestList[i].pause);
                tvTestList.Nodes[node.Index].Nodes.Add(childNode);
            }

            
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            message.Normal("Premere il tasto START per avviare il TEST");
            if (testList.TheFile == null )
            {
                message.Title("ERRORE");
                message.Error("Il file con la lista dei test non è stato trovato (setup.ini)");
            }
            lblTitle.Text = "";
            lblDescription.Text = "";
            lblCommand.Text = "";
            lblAnswer.Text = "";
            lblTimeout.Text = "";
            lblPause.Text = "";
            lblReceived.Text = "";
            
            FillTreeView();
        }

        string TestCassettesRevHome_Home(string[] cassettes)
        {
            this.BackColor = Color.FromArgb(88, 89, 91);
            Stopwatch myWatch = new Stopwatch();
            
            string result = "PASS";
            string answer = "";
            string[] address;
            int i = 0;
            lblTitle.Text = "REV-HOME / HOME CYCLE";
            lblCommand.Text = "Dn07 + Fn + Dn04 + Fn";
            lblAnswer.Text = "04/05/41";
            address = new string[cassettes.Length];
            for (i = 0; i < cassettes.Length; i++)
            {
                switch (cassettes[i].ToUpper())
                {
                    case "A":
                        address[i] = "4";
                        break;
                    case "B":
                        address[i] = "5";
                        break;
                    case "C":
                        address[i] = "6";
                        break;
                    case "D":
                        address[i] = "7";
                        break;
                    case "E":
                        address[i] = "8";
                        break;
                    case "F":
                        address[i] = "9";
                        break;
                    case "G":
                        address[i] = "A";
                        break;
                    case "H":
                        address[i] = "B";
                        break;
                    case "I":
                        address[i] = "C";
                        break;
                    case "J":
                        address[i] = "D";
                        break;
                    case "K":
                        address[i] = "E";
                        break;
                    case "L":
                        address[i] = "F";
                        break;
                }
                //REV HOME
                lblMessage.Text = "Avvio comando REV-HOME su cassetto " + cassettes[i] + " in corso";
                lblDescription.Text = "REV-HOME in corso";
                lstComScope.Items.Add("SND:D" + address[i] + "07");
                cmCommand.TransparentCommand("D" + address[i] + "07");
            }
            // avvio timer
            myWatch.Start();
            //verifica che tutti i cassetti siano FULL (04)
            verificaStatoFull:
            result = "PASS";
            for (i = 0; i < cassettes.Length; i++)
            {
                lblMessage.Text = "Richiesta stato del cassetto " + cassettes[i] + " in corso";
                lstComScope.Items.Add("SND:F" + address[i]);
                answer = cmCommand.TransparentCommand("F" + address[i]);
                lstComScope.Items.Add("RCV:" + answer);
                lstComScope.SelectedIndex = lstComScope.Items.Count - 1;
                if (answer!="04")
                {
                    result = "WAIT";
                }
                if (myWatch.Elapsed.Seconds > 300)
                {
                    result = "FAIL";
                    goto endTest;
                }
                utils.WaitinigTime(500);
            }
            if (result == "WAIT") goto verificaStatoFull;
            myWatch.Stop();
            myWatch.Reset();

            utils.WaitinigTime(5000);
            //HOME
            for (i = 0; i < cassettes.Length; i++)
            {
                lblMessage.Text = "Avvio comando HOME su cassetto " + cassettes[i] + " in corso";
                lblDescription.Text = "HOME in corso";
                lstComScope.Items.Add("SND:D" + address[i] + "04");
                cmCommand.TransparentCommand("D" + address[i] + "04");
            }

            //avvio Timer
            myWatch.Start();
            //verifica che tutti i cassetti siano EMPTY (05)
            verificaStatoEmpty:
            result = "PASS";
            for (i = 0; i < cassettes.Length; i++)
            {
                lblMessage.Text = "Richiesta stato del cassetto " + cassettes[i] + " in corso";
                lstComScope.Items.Add("SND:F" + address[i]);
                answer = cmCommand.TransparentCommand("F" + address[i]);
                lstComScope.Items.Add("RCV:" + answer);
                lstComScope.SelectedIndex = lstComScope.Items.Count - 1;
                if (answer != "05")
                {
                    result = "WAIT";
                }
                if (myWatch.Elapsed.Seconds > 300)
                {
                    result = "FAIL";
                    goto endTest;
                }
                utils.WaitinigTime(500);
            }
            if (result == "WAIT") goto verificaStatoEmpty;

            utils.WaitinigTime(5000);
            result = "PASS";
            endTest:
            myWatch.Stop();
            
            return result;
        }
        string TestExecute()
        {
            string result = "PASS";
            string reply = "";
            //for (int n = 0; n<tvTestList.Nodes.Count; n++)
            //{

            //}
            if (testList.GetValue("testparameters","onlytransparent").ToUpper()=="YES")
            {
                lstComScope.Items.Add("SND:X,1");
                reply = cmCommand.TransparentON();
                lstComScope.Items.Add("RCV:X,1," + cmCommand.OUTOPTRASPMODE.rc);
                lstComScope.SelectedIndex = lstComScope.Items.Count - 1;
            }
            for (int x = actualCicle; x <= totalCicle; x++)
            {
                if (stopTest == true)
                    break;
                lblCicleNumber.Text = x + " / " + totalCicle;
                for (int i = 0; i < cmTestList.Count(); i++)
                {
                    this.BackColor = Color.FromArgb(88, 89, 91);
                    tvTestList.Nodes[i].BackColor = Color.Gainsboro;
                    lblReceived.Text = "";
                    lblTitle.Text = cmTestList[i].title;
                    lblDescription.Text = cmTestList[i].description;
                    lblCommand.Text = cmTestList[i].command;
                    lblAnswer.Text = cmTestList[i].answer;
                    lblTimeout.Text = cmTestList[i].timeout.ToString();
                    lblPause.Text = cmTestList[i].pause.ToString();
                    switch(cmTestList[i].command)
                    {
                        case "TestCassettesRevHome_Home":
                            string[] myCassettes = cmTestList[i].answer.Split(',');
                            result = TestCassettesRevHome_Home(myCassettes);
                            break;
                        default:
                            lstComScope.Items.Add("SND:" + cmTestList[i].command);
                            if (cmTestList[i].command.IndexOf(",", 0) > 0)
                                reply = cmCommand.SingleCommand(cmTestList[i].command, cmTestList[i].timeout);
                            else
                                reply = cmCommand.TransparentCommand(cmTestList[i].command, cmTestList[i].timeout);
                            lstComScope.Items.Add("RCV:" + reply);
                            lblReceived.Text = reply;
                            if (reply != cmTestList[i].answer && cmTestList[i].answer != "")
                            {
                                tvTestList.Nodes[i].BackColor = Color.Red;
                                result = "FAIL";
                                goto endTest;
                            }
                            lstComScope.SelectedIndex = lstComScope.Items.Count - 1;
                            break;
                    }
                    utils.WaitinigTime(cmTestList[i].pause);
                    if (result == "FAIL") goto endTest;
                    tvTestList.Nodes[i].BackColor = Color.White;

                }
                testList.SetValue("testParameters", "actualCicleNumber", x.ToString());
                
            }
            endTest:
            if (testList.GetValue("testparameters", "onlytransparent").ToUpper() == "YES")
            {
                lstComScope.Items.Add("SND:D134");
                cmCommand.TransparentCommand("D134");
                lstComScope.Items.Add("SND:D234");
                cmCommand.TransparentCommand("D234");
                lstComScope.Items.Add("SND:Y,1");
                reply = cmCommand.TransparentOFF();
                lstComScope.Items.Add("RCV:Y,1," + cmCommand.OUTOPTRASPMODE.rc);
                lstComScope.SelectedIndex = lstComScope.Items.Count - 1;
                utils.WaitinigTime(3000);
            }
            return result;
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            string testResult = "";
            stopTest = false;
            lblMessage.Text = "";
            string reply = "";
            reply = cmCommand.Connect(ref cmCommand.CONNECTIONPARAM);
            if (reply != "OK")
            {
                message.Error("Connection FAILED, press any key to continue");
                utils.WaitingKey();
                goto endTest;
            }
            testResult = TestExecute();
            endTest:
            message.Title("TEST RIUSCITO CORRETTAMENTE");
            message.Error("Il test è terminato senza errori");
            cmCommand.Disconnect();
            if (testResult == "FAIL")
            {
                message.Title("TEST FALLITO");
                message.Error("Non è stato possibile portare a termine il test");
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (stopTest == false)
            {
                stopTest = true;
                lblStopTest.Text = "E' stato premuto il tasto STOP, al termine del ciclo corrente il test verrà interrotto";
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
